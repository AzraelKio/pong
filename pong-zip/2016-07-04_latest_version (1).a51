;Titel: 	   Pong
;Aufgabe:	Pong 
;Autoren:	Daniel Clappier und Fabius Balogh
;Datum      2016-07-05
include reg51.inc

JMP Init							; �bersprung der ISR

code at 03h    				; ISR0:	Spieler0(Links) nach unten
JMP Sp0DOWN
						
code at 13h						; ISR1:	Spieler0(Links) nach oben 
JMP Sp0UP
						
;_______________________________________________________________________________________
Init:
MOV TMOD,#0x01					; Timer0 als 16-Bit Timer aktivieren

SETB EX0							; Spieler0 Interrupts aktivieren + Taktflanken gesteuert
SETB EX1
SETB IT0
SETB IT1

MOV P0,#0x00					; Eingabe Spieler1 aktivieren (Hoch + Runter)
MOV P1,#0x00					; Matrix Y-Werte
MOV P2,#0x00					; Matrix X-Werte
                                   
MOV R0,#0x1C					; Spieler0 Start Position
MOV R1,#0x1C					; Spieler1 Start Position
MOV 0x20,#0x08					; Ball X-Position
MOV 0x21,#0x08 				; Ball Y-Position
SETB 0x10						; Richtungsbit f�r Ball, wenn gesetzt richtung Spieler1, wenn nicht zu Spieler0
CLR 0x11                  	; Ball prallt nach Oben, wenn gesetzt
CLR 0x12							; Ball prallt nach Unten, wenn gesetzt (Wir ermitteln die mitte im unterprogramm)	
MOV 0x23,#0x00					; Punkte f�r Spieler 0
MOV 0x24,#0x00					; Punkte f�r Spieler 1
MOV 0x25,#0x00					; Temp-Register f�r Ball abprallen an Balken

SETB EA							; Aktivierung aller Interrupts

;________________________________________________________________________________________
Main:								; Hauptprogramm
;----------------------
JNB P0.0,hilfe2				; Spieler1 (Rechter Balken 1 nach Oben)
call UpSpieler1
repeat1:
JB P0.0,repeat1				; Abfangen von mehrfacher ausfuehrung
hilfe2:
;----------------------
JNB P0.1,hilfe3				; Spieler1 (Rechter Balken 1 nach Unten)
call DownSpieler1
repeat2:
JB P0.1,repeat2				; Abfangen von mehrfach ausfuehrung
hilfe3:
;----------------------
CALL Ball						; Ball bewegen + Begrenzung Y-Achse
CALL BallPunkte            ; Punkte ermitteln + Ball Position zu Spieler Position + Begrenzung X-Achse
CALL AusgabeBall				; Ball anzeigen auf Matrix-Display
CALL AusgabeUser				; Beide User auf Matrix-Display anzeigen
JMP Main
;________________________________________________________________________________________

Ball:								; Ball bewegen + Begrenzung Y-Achse
MOV A,0x20						; Ball X-Richtung pruefen und entsprechend rotieren
JB 0x10,NaechsteSpalte
RR A
JMP ignor
NaechsteSpalte:
RL A
ignor:
MOV 0x20,A

MOV A,0x21						; Ball Y-Richtung pruefen und entsprechend rotieren, bei bedarf auch abprallen lassen
JNB 0x11,NaechsteAbfrage	; Soll der Ball nach oben gehen ?
MOV R4,0x21
CJNE R4,#0x01,weiter16 		; Hat Ball obere Kante erreicht?
CPL 0x11							
CPL 0x12
JMP NaechsteAbfrage
weiter16:
RR A

NaechsteAbfrage:
JNB 0x12,ZumEnde				; Soll der Ball nach unten gehen ?
MOV R4,0x21
CJNE R4,#0x80,weiter17 		; Hat Ball untere Kante erreicht?
CPL 0x11
CPL 0x12
JMP ZumEnde
weiter17:
RL A

ZumEnde:
MOV 0x21,A
RET
;________________________________________________________________________________________

BallPunkte:						; Punkte Logik: Der Abrallwinkel des Balls wird aufgrund des getroffenen Punktes des User-Balkens ermittelt			
JB 0x10,weiter8
MOV A,0x20						
CJNE A,#0x01,weiter9			
MOV A,0x21						; Ball in Richtung Spieler0
ANL A,R0
MOV 0x25,R0
CJNE A,#0x00,weiter10		; Abfrage 1: Ball treffer ins leere
INC 0x24							; PUNKT f�r Spieler1
MOV A,0x24
CJNE A,#0x05,weiter35		; ENDLOSSCHLEIFE: 5-Punkte ereicht, Spiel zu Ende und gewonnen
repeat5:
CALL BerechnePunkte
JMP repeat5
weiter35:
MOV 0x20,#0x08					; Reset Ball X-Position
MOV 0x21,#0x08					; Reset Ball Y-Position
CLR 0x10							; Reset Richtungsbits
CLR 0x11
CLR 0x12
CALL BerechnePunkte			; Berechnung + Ausgabe Spieler Punkte
weiter9:							; Label wird aufgrund von "Target out of Range" hierein gesetzt, damit returned werden kann, egal wo der Jump in dem UP erfolgt.
RET

weiter8:
MOV A,0x20						
CJNE A,#0x80,weiter9			; Ball in Richtung Spieler1
MOV A,0x21
ANL A,R1
MOV 0x25,R1 
CJNE A,#0x00,weiter10		; Abfrage 1: Ball treffer ins leere
INC 0x23							; PUNKT f�r Spieler0
MOV A,0x23
CJNE A,#0x05,weiter36		; ENDLOSSCHLEIFE: 5-Punkte ereicht Spiel zu Ende und gewonnen
repeat6:
CALL BerechnePunkte
JMP repeat6
weiter36:
MOV 0x20,#0x08					; Reset Ball X-Position
MOV 0x21,#0x08					; Reset Ball Y-Position
SETB 0x10						; Reset Richtungsbits
CLR 0x11
CLR 0x12
CALL BerechnePunkte			; Berechnung + Ausgabe Spieler Punkte
RET

weiter10:
RR A
ANL A,0x25
CJNE A,#0x00,weiter11		; Abfrage2 Ball im leeren			OBERES BIT BALKEN
SETB 0x11
CLR 0x12
JMP weiter14

weiter11:
RR A
ANL A,0x25
CJNE A,#0x00,weiter12		; Abfrage3 Ball nun im leeren		MITTLERES BIT BALKEN
CLR 0x11
CLR 0x12
JMP weiter14
RET

weiter12:
RR A
CJNE A,#0x00,weiter13		; Abfrage4 Ball nun im leeren 	UNTERES BIT BALKEN
JMP weiter9
weiter13:
CLR 0x11
SETB 0x12
weiter14:
CPL 0x10
JB 0x10,weiter15
MOV A,0x20
RR A
MOV 0x20,A
JMP weiter9
weiter15:
MOV A,0x20
RL A
MOV 0x20,A
RET								; Final Return
;________________________________________________________________________________________

AusgabeBall:					; Zeigt Ball auf Matrix-Display an
MOV P2, #0x00					; Anzeige Aus
MOV P1, 0x21  				 	; Y-Achse
MOV P2, 0x20					; X-Achse
MOV TL0,#0x88					; Timer laden mit 5000
MOV TH0,#0x13
SETB TR0							; Timer0 starten
again5:
JB TF0,weiter18				; PAUSE Warte auf Timerueberlauf
JMP again5
weiter18:
CLR TR0							; Stoppe Timer + Reset Ueberlaufbit
CLR TF0
RET

;________________________________________________________________________________________

AusgabeUser:					; Ausgabe von beiden Spieler Balken 
MOV P1, R0						; Spieler0 auf Y-Achse
MOV P2, #0x01              ; X Position
MOV TL0,#0xFF					; Timer laden mit 64511
MOV TH0,#0xFB
SETB TR0 						; Timer0 start
again2:
JB TF0,weiter19				; PAUSE Warte auf Timerueberlauf
JMP again2
weiter19:
CLR TR0							; Stoppe Timer + Reset Ueberlaufbit
CLR TF0
MOV P2, #0x00					; Anzeige aus

MOV P1, R1             		; Spieler1 auf Y-Achse
MOV P2, #0x80					; X Position
MOV TL0,#0xFF					; Timer0 laden mit 64511
MOV TH0,#0xFB
SETB TR0							; Timer0 start 
again4:
JB TF0,weiter20				; PAUSE Warte auf Timerueberlauf
JMP again4
weiter20:
CLR TR0							; Stoppe Timer + Reset Ueberlaufbit
CLR TF0
MOV P2, #0x00          		; Anzeige aus
RET

;________________________________________________________________________________________

DownSpieler1:					; Spieler1 nach unten steuern
CJNE R1,#0xE0,hilf6			; Pruefe ob au�erhalb des Feldes
JMP ueberspringen4	
hilf6:
MOV A,R1                   ; Verschiebe nach unten
RL A								
MOV R1,A
ueberspringen4:
RET


;________________________________________________________________________________________

UpSpieler1:						; Spieler1 nach oben steuern
CJNE R1,#0x07,hilf5			; Pruefe ob au�erhalb des Feldes
JMP ueberspringen3	
hilf5:	
MOV A,R1							; Verschiebe nach oben
RR A
MOV R1,A
ueberspringen3:
RET

;________________________________________________________________________________________

Sp0DOWN:							; Spieler0 nach unten steuern
CJNE R0,#0xE0,hilf			; Pruefe ob au�erhalb des Feldes
JMP ueberspringen
hilf:	
MOV A,R0
RL A								; Verschiebe nach unten
MOV R0,A
ueberspringen:
RETI

;________________________________________________________________________________________

Sp0UP:							; Spieler0 nach oben steuern
CJNE R0,#0x07,hilf1			; Pruefe ob au�erhalb des Feldes
JMP ueberspringen1
hilf1:
MOV A,R0							; Verschiebe nach oben
RR A
MOV R0,A
ueberspringen1:
RETI

;________________________________________________________________________________________

BerechnePunkte:				; Nimmt Punkte der Spieler und erstellt Punkte-Balken und gibt diesen Aus.
MOV 0x2E,0x23					; Punkte Spieler0 temporaer gespeichert
MOV 0x2F,0x24					; Punkte Spieler1 temporaer gespeichert

MOV DPTR,#tab					; Spieler0 Hole Punkte-Balken entsprechend der Punkte aus Tabelle und schreibe ihn auf die temporaere Variable
MOV A,0x2E
MOVC A,@A+DPTR
MOV 0x2E,A

MOV DPTR,#tab					; Spieler1 Hole Punkte-Balken entsprechend der Punkte aus Tabelle und schreibe ihn auf die temporaere Variable
MOV A,0x2F
MOVC A,@A+DPTR
MOV 0x2F,A

MOV 0x2C,#0x01					; Pausen-Schleife fuer Ausgabe der Punkte
schleife6:
MOV 0x2B,#0xFF
schleife5:
MOV 0x2A,#0xFF	
schleife4:

CALL AusgabePunkte

DJNZ 0x2A,schleife4
DJNZ 0x2B,schleife5
DJNZ 0x2C,schleife6

RET

;________________________________________________________________________________________

AusgabePunkte:					; Gibt Punkte der Spieler auf Display aus

MOV P1, #0x00					; Anzeige Aus
MOV P2, #0x00					
MOV P1, 0x2E  					; Spieler0 Y-Achse
MOV P2, #0x01					; X-Achse

MOV P2, #0x00				 	; Anzeige Aus
MOV P1, #0x00					

MOV P1, 0x2F  					; Spieler1 Y-Achse
MOV P2, #0x80					; X-Achse

MOV P2, #0x00					; Anzeige Aus
MOV P1, #0x00					 

RET

;________________________________________________________________________________________

;Tabelle f�r Punktausgabe
tab: DB 0x00,0x80,0xC0,0xE0,0xF0,0xF8
