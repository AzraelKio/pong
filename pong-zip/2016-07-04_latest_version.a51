;Titel: 	   Pong
;Aufgabe:	Pong 
;Autoren:	Daniel Clappier und Fabius Balogh
;Datum      2016-07-04 
include reg51.inc
JMP Init
code at 03h
JMP Sp0DOWN
						;Spieler0Links nach unten
code at 13h
JMP Sp0UP

						;Spieler0Links nach oben 
;_______________________________________________________________________________________
Init:
MOV TMOD,#0x01					;Timer0 als 16-Bit Timer aktivieren
MOV TL0,#0x88					;Timer laden mit 49996
MOV TH0,#0x13


Setb EX1
Setb Ex0
Setb It0
Setb IT1

Mov P0,#0x00					; Hoch und runter Spieler1 aktivieren
Mov P1,#0x00					; Matrix Spalte
Mov P2,#0x00					; Matrix Reihe
                                   
Mov R0,#0x1C					; Spieler0 Start possition
Mov R1,#0x1C					; Spieler1 Start possition
Mov R3,#0x01					; Refreschregister (rotiert
Mov 0x20,#0x08					; Ball X-Position
MOV 0x21,#0x08 				; Ball Y-Position
SETB 0x10							; richtungsbit f�r ball in reg 22 wenn gesetzt richtung Spieler 1
CLR 0x11                  	; Ball Prallt nach oben
CLR 0x12							; Ball Prallt nach Unten (Wir ermitteln die mitte im unterprogramm)	
Mov 0x23,#0x00					; Punkte f�r Spieler 0
Mov 0x24,#0x00					; Punkte f�r Spieler 1
MOV 0x25,#0x00					; Temp-Register f�r Ball abprallen an Balken
SETB EA							; Aktivierung aller Interrupts
;________________________________________________________________________________________
Main:
;----------------------
JNB P0.0,hilfe2				;Rechter balken nach oben
call upSpieler1
hilfe2:
;----------------------
JNB P0.1,hilfe3				;Rechter balken nach unten
call DownSpieler1
hilfe3:
;----------------------
CALL Ball
CALL BallPunkte
CALL AusgabeBall
CALL AusgabeUser
JMP Main
;________________________________________________________________________________________

Ball:
Mov A,0x20
JB 0x10,NaechsteSpalte
RR A
JMP ignor
NaechsteSpalte:
RL A
ignor:
Mov 0x20,A
Mov A,0x21
JNB 0x11,NaechsteAbfrage
MOV R4,0x21
CJNE R4,#0x01,weiter16 
CPL 0x11
CPL 0x12
JMP NaechsteAbfrage
weiter16:
RR A
NaechsteAbfrage:
JNB 0x12,ZumEnde
MOV R4,0x21
CJNE R4,#0x80,weiter17 
CPL 0x11
CPL 0x12
JMP ZumEnde
weiter17:
RL A
ZumEnde:
Mov 0x21,A
RET

BallPunkte:
;----------------------				; Punkte Logik: Der Abrallwinkel des Balls wird aufgrund des getroffenen Punktes des User-Balkens ermittelt
JB 0x10,weiter8
MOV A,0x20								;Fehler vermeidung fuer CJNE
CJNE A,#0x01,weiter9			
MOV A,0x21							; ball richtung spieler 0
ANL A,R0
MOV 0x25,R0
CJNE A,#0x00,weiter10
INC 0x24
Mov 0x20,#0x08					; Reset Ball X-Position
MOV 0x21,#0x08					; Reset Ball Y-Position
CLR 0x10							; Reset Richtungsbits
CLR 0x11
CLR 0x12
call UPPause
call UPPause
RET
weiter8:
MOV A,0x20								;Fehler vermeidung fuer CJNE
CJNE A,#0x80,weiter9					; ball richtung spieler 1
MOV A,0x21
ANL A,R1
MOV 0x25,R1 
CJNE A,#0x00,weiter10				; Abfrage1 BallBit im leeren
INC 0x23
Mov 0x20,#0x08					; Reset Ball X-Position
MOV 0x21,#0x08					; Reset Ball Y-Position
SETB 0x10						; Reset Richtungsbits
CLR 0x11
CLR 0x12
call UPPause
call UPPause
RET
weiter10:
RR A
ANL A,0x25
CJNE A,#0x00,weiter11				; Abfrage2 BallBit im leeren		OBERES BIT BALKEN
SETB 0x11
CLR 0x12
JMP weiter14
weiter11:
RR A
ANL A,0x25
CJNE A,#0x00,weiter12				; Abfrage3 BallBit nun im leeren	MITTLERES BIT BALKEN
CLR 0x11
CLR 0x12
JMP weiter14
RET
weiter12:
RR A
CJNE A,#0x00,weiter13				; Abfrage4 BallBit nun im leeren UNTERES BIT BALKEN
JMP weiter9
weiter13:
CLR 0x11
SETB 0x12
weiter14:
CPL 0x10
JB 0x10,weiter15
MOV A,0x20
RR A
MOV 0x20,A
JMP weiter9
weiter15:
MOV A,0x20
RL A
MOV 0x20,A
weiter9:
RET										; Final Exit
;________________________________________________________________________________________
AusgabeBall:
MOV P2, #0x00				;Anzeige Aus
MOV P1, 0x21  				;Y-Achse
MOV P2, 0x20				;X-Achse
MOV TL0,#0x88							;Timer laden mit 49996
MOV TH0,#0x13
SETB TR0						;Timer0 starten
again5:
JB TF0,weiter18
JMP again5
weiter18:
CLR TR0
CLR TF0
RET

;________________________________________________________________________________________
AusgabeUser:							;Ausgabe von beiden Balken 
MOV P1, R0								;Spieler 0 
MOV P2, #0x01
MOV TL0,#0xFF							;Timer laden mit 49996
MOV TH0,#0xFB
SETB TR0 								;Timer0 start
again2:
JB TF0,weiter19
JMP again2
weiter19:
CLR TR0
CLR TF0
MOV P2, #0x00
MOV P1, R1                    	;Spieler 1
MOV P2, #0x80
MOV TL0,#0xFF					;Timer0 laden mit 49996
MOV TH0,#0xFB
SETB TR0							;Timer0 start 
again4:
JB TF0,weiter20
JMP again4
weiter20:
CLR TR0
CLR TF0
MOV P2, #0x00              	

RET

;________________________________________________________________________________________
DownSpieler1:					;rechts nach unten steuern
Cjne R1,#0xe0,hilf6
JMP ueberspringen4	
hilf6:		
MOV TL0,#0xFF					;Timer0 laden mit 49996
MOV TH0,#0xFA
SETB TR0							;Timer0 start 
again6:
JB TF0,weiter21
JMP again6
weiter21:
CLR TR0
CLR TF0	
Mov A,R1
RL A
Mov R1,A
ueberspringen4:
ret


;________________________________________________________________________________________
upSpieler1:						;rechts nach oben steuern
Cjne R1,#0x07,hilf5
JMP ueberspringen3	
hilf5:	
MOV TL0,#0xFF					;Timer0 laden mit 49996
MOV TH0,#0xFA
SETB TR0							;Timer0 start 
again7:
JB TF0,weiter22
JMP again7
weiter22:
CLR TR0
CLR TF0			
Mov A,R1
RR A
Mov R1,A
ueberspringen3:
ret

;________________________________________________________________________________________
Sp0DOWN:							;Linker balken nach unten
Cjne R0,#0xe0,hilf
JMP ueberspringen
hilf:	
Mov A,R0
RL A
MOV R0,A
ueberspringen:
RETI

;________________________________________________________________________________________
Sp0UP:							;Linker balken nach oben
Cjne R0,#0x07,hilf1
JMP ueberspringen1
hilf1:
Mov A,R0
RR A
MOV R0,A
ueberspringen1:
RETI
;________________________________________________________________________________________
UPPause:    					;Pause f�r neuerscheinen des Balles
MOV TL0,#0x00							;Timer laden mit 49996
MOV TH0,#0x00
SETB TR0						;Timer0 starten
again1:
JB TF0,weiter23
JMP again1
weiter23:
CLR TR0
CLR TF0
ret
