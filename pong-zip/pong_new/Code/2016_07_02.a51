;Titel: 	   Pong
;Aufgabe:	Pong 
;Autoren:	Daniel Clappier und Fabius Balogh
;Datum      2016-06-27 
include reg51.inc
JMP Init
code at 03h
JMP Sp0DOWN						;Spieler0Links nach unten
code at 0Bh					;TimerInterrupt0 Aktualisierung User
;CALL AusgabeUser
RETI
code at 13h
JMP Sp0UP						;Spieler0Links nach oben
code at 1Bh					;TimerInterrupt1 Aktualisierung Ball
;CALL AusgabeBall
RETI
;_______________________________________________________________________________________
Init:
CLR ET0							;Aktivierung TInterrupt0 User
CLR ET1							;---"------------"------ Ball
MOV TMOD,#0x22					;Beide Timer mit 8Bit Autoreload
MOV TL0,#0x0F					;Timer laden
MOV TH0,#0x0F
MOV TL1,#0x3F
MOV TH1,#0x3F

Setb EX1
Setb Ex0

;Mov P0,#0x00					;Hoch und runter Spieler1 
CLR p0.5
Mov P1,#0x00					; Matrix Spalte
Mov P2,#0x00					; Matrix Reihe
                                   
Mov R0,#0x1C					; Spieler0 Start possition
Mov R1,#0x1C					; Spieler1 Start possition
Mov R3,#0x01					; Refreschregister (rotiert
Mov 0x20,#0x08	,				; Ball X-Position
MOV 0x21,#0x08 				; Ball Y-Position
CLR 0x10						; richtungsbit f�r ball in reg 22 wenn gesetzt richtung Spieler 1
CLR 0x11                  	; Ball Pralltt nach oben
CLR 0x12							; Ball Prallt nach Unten (Wir ermitteln die mitte im unterprogramm)	
Mov 0x23,#0x00					; Punkte f�r Spieler 0
Mov 0x24,#0x00					; Punkte f�r Spieler 1
MOV 0x25,#0x00					; Temp-Register f�r Ball abprallen an Balken
SETB EA							;Aktivierung aller Interrupts
MOV TCON,#0x50					;Beide Timer starten
;________________________________________________________________________________________
Main:
;----------------------
JNB P0.0,hilfe2				;Rechter balken nach oben
call upSpieler1
hilfe2:
;----------------------
JNB P0.1,hilfe3				;Rechter balken nach unten
call DownSpieler1
hilfe3:
;----------------------
Call Ball
CALL AusgabeBall
CALL AusgabeUser
MOV A,0x24
CJNE A,#0x01, xd
SETB P0.5
xd:
JMP Main
;________________________________________________________________________________________

Ball:
;----------------------	Ball auf X-Achse versetzen
MOV A,0x20
JB 0x10, weiter1
CJNE A,#0x02,weiter2
JMP uebersprung1
weiter2:
RR A
JMP uebersprung2
weiter1:
CJNE A,#0x40,weiter3
JMP uebersprung1
weiter3:
RL A
uebersprung2:
MOV 0x20,A
uebersprung1:
;----------------------	Ball auf Y-Achse versetzen
MOV A,0x21
JNB 0x11, weiter4
CJNE A,#0x80,weiter5
JMP weiter4
weiter5:
RR A
CPL 0x11
CPL 0x12
weiter4:
JNB 0x12, weiter6
CJNE A,#0x01,weiter7
JMP weiter6
weiter7:
RL A
CPL 0x11
CPL 0x12
weiter6:
MOV 0x21,A
;----------------------				; Punkte Logik: Der Abrallwinkel des Balls wird aufgrund des getroffenen Punktes des User-Balkens ermittelt
JNB 0x10,weiter8
MOV A,0x20								;Fehler vermeidung fuer CJNE
CJNE A,#0x02,weiter9				; Weiter is not creativ! (not green)
MOV A,0x20
ANL A,R0
MOV 0x25,R0
CJNE A,#0x00,weiter10
INC 0x24
RET
weiter8:
MOV A,0x20								;Fehler vermeidung fuer CJNE
CJNE A,#0x40,weiter9
MOV A,0x21
ANL A,R1
MOV 0x25,R1 
CJNE A,#0x00,weiter10
INC 0x23
RET
weiter10:
RR A
ANL A,0x25
CJNE A,#0x00,weiter11
SETB 0x11
CLR 0x12
CPL 0x10
RET
weiter11:
RR A
CJNE A,#0x00,weiter12
CLR 0x11
CLR 0x12
CPL 0x10
RET
weiter12:
RR A
CJNE A,#0x00,weiter13
JMP weiter9
weiter13:
CLR 0x11
SETB 0x12
CPL 0x10
weiter9:
RET
;________________________________________________________________________________________
AusgabeBall:
MOV P2, #0x00				;Anzeige Aus
MOV P1, 0x21  				;Y-Achse
MOV P2, 0x20				;X-Achse
RET

;________________________________________________________________________________________
AusgabeUser:							;Ausgabe von beiden Balken 
MOV P2, #0x00
MOV P1, R0								;Spieler 0 
MOV P2, #0x01
MOV P2, #0x00
MOV P1, R1                    	;Spieler 1
MOV P2, #0x80               	

RET

;________________________________________________________________________________________
DownSpieler1:					;rechts nach unten steuern
Cjne R1,#0xe0,hilf6
JMP ueberspringen4	
hilf6:			
Mov A,R1
RL A
Mov R1,A
ueberspringen4:
ret


;________________________________________________________________________________________
upSpieler1:						;rechts nach oben steuern
Cjne R1,#0x07,hilf5
JMP ueberspringen3	
hilf5:				
Mov A,R1
RR A
Mov R1,A
ueberspringen3:
ret

;________________________________________________________________________________________
Sp0DOWN:							;Linker balken nach unten
Cjne R0,#0xe0,hilf
JMP ueberspringen
hilf:
Mov A,R0
RL A
MOV R0,A
ueberspringen:
reti

;________________________________________________________________________________________
Sp0UP:							;Linker balken nach oben
Cjne R0,#0x07,hilf1
JMP ueberspringen1
hilf1:
Mov A,R0
RR A
MOV R0,A
ueberspringen1:
reti
;________________________________________________________________________________________
