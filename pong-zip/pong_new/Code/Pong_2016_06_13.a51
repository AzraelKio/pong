;Titel: 	   Pong
;Aufgabe:	Pong 
;Autoren:	Daniel Clappier und Fabius Balogh
;Datum      2016-06-13 
include reg51.inc
JMP Init
code at 0003h
JMP Sp0DOWN					;Spieler0Links nach unten
code at 0013h
JMP Sp0UP						;Spieler0Links nach oben
;_______________________________________________________________________________________
Init:
Setb EA
Setb EX1
Setb Ex0

Mov P0,#0x00				;Hoch und runter Spieler1 

Mov P1,#0x00				; Matrix Spalte
Mov P2,#0x00				; Matrix Reihe
                                    
Mov R0,#0x1C				; Spieler0 Start possition
Mov R1,#0x1C				; Spieler1 Start possition
Mov R3,#0x01				; Refreschregister (rotiert)
Mov 0x20,#0x08				; Ball X-Position
MOV 0x21,#0x09 			; Ball Y-Position
 
;________________________________________________________________________________________
Main:
;----------------------
JNB P0.0,hilfe2			;Rechter balken nach oben
call upSpieler1
hilfe2:
;----------------------
JNB P0.1,hilfe3			;Rechter balken nach unten
call DownSpieler1
hilfe3:
;----------------------
Call AusgabeUser
Call Ball

JMP Main
;________________________________________________________________________________________
Ball:
MOV P1, 0x20  				;X-Achse
MOV P2, 0x21				;Y-Achse
MOV P2, #0x00				;Anzeige Aus
RET

;________________________________________________________________________________________
AusgabeBall:
MOV P1, 0x20  				;X-Achse
MOV P2, 0x21				;Y-Achse
MOV P2, #0x00				;Anzeige Aus
RET

;________________________________________________________________________________________
AusgabeUser:
MOV P1, R0
MOV P2, #0x01
MOV P2, #0x00
MOV P1, R1
MOV P2, #0x80
MOV P2, #0x00
RET

;________________________________________________________________________________________
DownSpieler1:					;rechts nach unten steuern
Cjne R1,#0xe0,hilf6
JMP ueberspringen4	
hilf6:			
Mov A,R1
RR A
Mov R1,A
ueberspringen4:
ret


;________________________________________________________________________________________
upSpieler1:						;rechts nach oben steuern
Cjne R1,#0x07,hilf5
JMP ueberspringen3	
hilf5:				
Mov A,R1
Rl A
Mov R1,A
ueberspringen3:
ret

;________________________________________________________________________________________
Sp0DOWN:						;Linker balken nach unten
Cjne R0,#0xe0,hilf
JMP ueberspringen
hilf:
Mov A,R0
RR A
MOV R0,A
ueberspringen:
reti

;________________________________________________________________________________________
Sp0UP:						;Linker balken nach oben
Cjne R0,#0x07,hilf1
JMP ueberspringen1
hilf1:
Mov A,R0
RL A
MOV R0,A
ueberspringen1:
reti
;________________________________________________________________________________________
