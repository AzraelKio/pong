ORG 00h
JMP Init                            ; Übersprung der ISR

ORG 03h                             ; ISR0:    Spieler0(Links) nach unten
JMP ifunc_movePaddleP0

ORG 13h                             ; ISR1:    Spieler0(Links) nach oben 
JMP ifunc_movePaddleP0

;_______________
; Definitions:
BallX       DATA 0x20
BallXStart  BIT  BallX.0
BallXEnd    BIT  BallX.7

BallY       DATA 0x21
BallYStart  BIT  BallY.0
BallYEnd    BIT  BallY.7

BallMove    DATA 0x22
BallDir     Bit BallMove.0
BallUp      Bit BallMove.1
BallDown    Bit BallMove.2

Buttons      EQU P0
ButtonP1Up   BIT Buttons.0          ; Low = Pressed (?)
ButtonP1Down BIT Buttons.1          ; Low = Pressed (?)

ButtonP0Up   BIT P3.2               ; High = Pressed
ButtonP0Down BIT P3.3               ; High = Pressed

PaddleP0        DATA 0x26
PaddleP0Top     BIT PaddleP0.0
PaddleP0Bottom  BIT PaddleP0.7
PaddleP1        DATA 0x27
PaddleP1Top     BIT PaddleP1.0
PaddleP1Bottom  BIT PaddleP1.7

PointsP0 DATA 0x23
PointsP1 DATA 0x24

DispX EQU P1
DispY EQU P2
;_______________________________________________________________________________________
Init:
MOV TMOD, #0x01                     ; Timer0 als 16-Bit Timer aktivieren

SETB EX0                            ; Spieler0 Interrupts aktivieren + Taktflanken gesteuert
SETB EX1
SETB IT0
SETB IT1

;MOV Buttons, #0x00                 ; Eingabe Spieler1 aktivieren (Hoch + Runter)
MOV DispX, #0x00                    ; Matrix Y-Werte
MOV DispY, #0x00                    ; Matrix X-Werte

MOV PaddleP0, #0x1C                 ; Spieler0 Start Position
MOV PaddleP1, #0x1C                 ; Spieler1 Start Position
MOV BallX, #0x08                    ; Ball X-Position
MOV BallY, #0x08                    ; Ball Y-Position
SETB BallDir                        ; Richtungsbit für Ball, wenn gesetzt richtung Spieler1, wenn nicht zu Spieler0
CLR BallUp                          ; Ball prallt nach Oben, wenn gesetzt
CLR BallDown                        ; Ball prallt nach Unten, wenn gesetzt (Wir ermitteln die mitte im unterprogramm)    
MOV PointsP0, #0x00                 ; Punkte für Spieler 0
MOV PointsP1, #0x00                 ; Punkte für Spieler 1
MOV 0x25, #0x00                     ; Temp-Register für Ball abprallen an Balken

SETB EA                             ; Aktivierung aller Interrupts

;________________________________________________________________________________________
Main:                               ; Hauptprogramm
CALL func_movePaddleP1              ; Poll P1 Buttons and move paddle
CALL func_moveBall                  ; Ball bewegen + Begrenzung Y-Achse
CALL BallPunkte                     ; Punkte ermitteln + Ball Position zu Spieler Position + Begrenzung X-Achse
CALL AusgabeBall                    ; Ball anzeigen auf Matrix-Display
CALL AusgabeUser                    ; Beide User auf Matrix-Display anzeigen
JMP Main

;________________________________________________________________________________________

func_moveBall:                      ; Move Ball and invert direction if upper/lower border is reached

MOV A, BallX                        ; x = ballX
JB BallDir, j_mb_goRight            ; Is ball going left? else goRight
RR A                                ;   Rotate left
JMP j_mb_movX                       ;   skip goRight
j_mb_goRight:                       ; goRight:
RL A                                ;   Rorate right
j_mb_movX:                          ;
MOV BallX, A                        ; Write back

MOV A, BallY                        ; x = ballY
JNB BallUp, j_mb_skipUp             ; Is ball going up?
RR A                                ;   Rotate up
j_mb_skipUp:                        ; 
JNB BallDown, j_mb_skipDown         ; Is ball going down?
RL A                                ;   Rotate down
j_mb_skipDown:                      ;
MOV BallY, A                        ; ballY = x

ANL A, #10000001b                   ; Apply border bitmask to ball position
JZ j_mb_skipBounce                  ; Is a bit set? (is the ball at top/bottom)
CPL BallUp                          ;   Invert BallUp
CPL BallDown                        ;   Invert BallDown
j_mb_skipBounce:                    ;
RET                                 ; Return

;________________________________________________________________________________________

BallPunkte:                         ; Punkte Logik: Der Abrallwinkel des Balls wird aufgrund des getroffenen Punktes des User-Balkens ermittelt            
JB BallDir, weiter8
MOV A, BallX                        
CJNE A, #0x01, weiter9            
MOV A, BallY                        ; Ball in Richtung Spieler0
ANL A, PaddleP0
MOV 0x25, PaddleP0
CJNE A, #0x00, weiter10             ; Abfrage 1: Ball treffer ins leere
INC PointsP1                        ; PUNKT für Spieler1
MOV A, PointsP1
CJNE A, #0x05, weiter35             ; ENDLOSSCHLEIFE: 5-Punkte ereicht, Spiel zu Ende und gewonnen
repeat5:
CALL BerechnePunkte
JMP repeat5
weiter35:
MOV BallX, #0x08                    ; Reset Ball X-Position
MOV BallY, #0x08                    ; Reset Ball Y-Position
CLR BallDir                         ; Reset Richtungsbits
CLR BallUp
CLR BallDown
CALL BerechnePunkte                 ; Berechnung + Ausgabe Spieler Punkte
weiter9:                            ; Label wird aufgrund von "Target out of Range" hierein gesetzt, damit returned werden kann, egal wo der Jump in dem UP erfolgt.
RET

weiter8:
MOV A, BallX                        
CJNE A, #0x80, weiter9              ; Ball in Richtung Spieler1
MOV A, BallY
ANL A, PaddleP1
MOV 0x25, PaddleP1 
CJNE A, #0x00, weiter10             ; Abfrage 1: Ball treffer ins leere
INC PointsP0                        ; PUNKT für Spieler0
MOV A, PointsP0
CJNE A, #0x05, weiter36             ; ENDLOSSCHLEIFE: 5-Punkte ereicht Spiel zu Ende und gewonnen
repeat6:
CALL BerechnePunkte
JMP repeat6
weiter36:
MOV BallX, #0x08                    ; Reset Ball X-Position
MOV BallY, #0x08                    ; Reset Ball Y-Position
SETB BallDir                        ; Reset Richtungsbits
CLR BallUp
CLR BallDown
CALL BerechnePunkte                 ; Berechnung + Ausgabe Spieler Punkte
RET

weiter10:
RR A
ANL A, 0x25
CJNE A, #0x00, weiter11             ; Abfrage2 Ball im leeren            OBERES BIT BALKEN
SETB BallUp
CLR BallDown
JMP weiter14

weiter11:
RR A
ANL A, 0x25
CJNE A, #0x00, weiter12             ; Abfrage3 Ball nun im leeren        MITTLERES BIT BALKEN
CLR BallUp
CLR BallDown
JMP weiter14
RET

weiter12:
RR A
CJNE A, #0x00, weiter13             ; Abfrage4 Ball nun im leeren     UNTERES BIT BALKEN
JMP weiter9
weiter13:
CLR BallUp
SETB BallDown
weiter14:
CPL BallDir
JB BallDir, weiter15
MOV A, BallX
RR A
MOV BallX, A
JMP weiter9
weiter15:
MOV A, BallX
RL A
MOV BallX, A
RET                                 ; Final Return
;________________________________________________________________________________________

AusgabeBall:                        ; Zeigt Ball auf Matrix-Display an
MOV DispX, BallY                    ; Y-Achse
MOV DispY, BallX                    ; X-Achse
MOV DispY, #0x00
MOV TL0, #0xF0                      ; Timer laden mit 5000
MOV TH0, #0xFF
SETB TR0                            ; Timer0 starten
again5:
JB 8Dh, weiter18                    ; PAUSE Warte auf Timerueberlauf
JMP again5
weiter18:
CLR TR0                             ; Stoppe Timer + Reset Ueberlaufbit
CLR TF0
RET

;________________________________________________________________________________________

AusgabeUser:                        ; Ausgabe von beiden Spieler Balken 
MOV DispX, PaddleP0                 ; Spieler0 auf Y-Achse
MOV DispY, #0x01                    ; X Position
MOV DispY, #0x00
MOV TL0, #0xF0                      ; Timer laden mit 64511
MOV TH0, #0xFF
SETB TR0                            ; Timer0 start
again2:
JB TF0, weiter19                    ; PAUSE Warte auf Timerueberlauf
JMP again2
weiter19:
CLR TR0                             ; Stoppe Timer + Reset Ueberlaufbit
CLR TF0

MOV DispX, PaddleP1                 ; Spieler1 auf Y-Achse
MOV DispY, #0x80                    ; X Position
MOV DispY, #0x00
MOV TL0, #0xF0                      ; Timer0 laden mit 64511
MOV TH0, #0xFF
SETB TR0                            ; Timer0 start 
again4:
JB TF0, weiter20                    ; PAUSE Warte auf Timerueberlauf
JMP again4
weiter20:
CLR TR0                             ; Stoppe Timer + Reset Ueberlaufbit
CLR TF0
RET

;________________________________________________________________________________________

func_movePaddleP1:                  ; Function: Check Buttons of P1 and move paddle
MOV A, PaddleP1                     ; x = paddle
j_mpp1_up:                          ;
JB ButtonP1Up, j_mpp1_down          ; Is up pressed? else check down
JB PaddleP1Top, j_mpp1_return       ;   Is paddle not at top? else return
RR A                                ;     rotate paddle(x) up;
JMP j_mpp1_common                   ;     skip down check  
j_mpp1_down:                        ; 
JB ButtonP1Down, j_mpp1_return      ; Is down pressed? else return
JB PaddleP1Bottom, j_mpp1_return    ;   Is paddle not at bottom? else return
RL A                                ;       rotate paddle(x) down;
j_mpp1_common:                      ; 
MOV PaddleP1, A                     ; paddle = x
j_mpp1_return:                      ;
RET                                 ;

;________________________________________________________________________________________

ifunc_movePaddleP0:                 ; Interrupt Function: Check Buttons of P0 and move paddle
PUSH A                              ; save acc
MOV A, PaddleP0                     ; x = paddle
j_mpp0_up:                          ;
JB ButtonP0Up, j_mpp0_down         ; Is up pressed? else check down
JB PaddleP0Top, j_mpp0_return       ;   Is paddle not at top? else return
RR A                                ;     rotate paddle(x) up;
JMP j_mpp0_common                   ;     skip down check  
j_mpp0_down:                        ; 
JB ButtonP0Down, j_mpp0_return     ; Is down pressed? else return
JB PaddleP0Bottom, j_mpp0_return    ;   Is paddle not at bottom? else return
RL A                                ;       rotate paddle(x) down;
j_mpp0_common:                      ; 
MOV PaddleP0, A                     ; paddle = x
j_mpp0_return:                      ; 
POP A                               ; restore acc
RETI                                ;

;________________________________________________________________________________________

BerechnePunkte:                     ; Nimmt Punkte der Spieler und erstellt Punkte-Balken und gibt diesen Aus.
MOV 0x2E, PointsP0                  ; Punkte Spieler0 temporaer gespeichert
MOV 0x2F, PointsP1                  ; Punkte Spieler1 temporaer gespeichert

MOV DPTR, #tab                      ; Spieler0 Hole Punkte-Balken entsprechend der Punkte aus Tabelle und schreibe ihn auf die temporaere Variable
MOV A, 0x2E
MOVC A, @A+DPTR
MOV 0x2E, A

MOV DPTR, #tab                      ; Spieler1 Hole Punkte-Balken entsprechend der Punkte aus Tabelle und schreibe ihn auf die temporaere Variable
MOV A, 0x2F
MOVC A, @A+DPTR
MOV 0x2F, A

MOV 0x2C, #0x05                     ; Pausen-Schleife fuer Ausgabe der Punkte
schleife6:
MOV 0x2B, #0x01
schleife5:
MOV 0x2A, #0x01    
schleife4:

CALL AusgabePunkte

DJNZ 0x2A, schleife4
DJNZ 0x2B, schleife5
DJNZ 0x2C, schleife6

RET

;________________________________________________________________________________________

AusgabePunkte:                      ; Gibt Punkte der Spieler auf Display aus
                    
MOV DispX, 0x2E                     ; Spieler0 Y-Achse
MOV DispY, #0x01                    ; X-Achse
MOV DispY, #0x00                    ; Anzeige Aus                

MOV DispX, 0x2F                     ; Spieler1 Y-Achse
MOV DispY, #0x80                    ; X-Achse
MOV DispY, #0x00                    ; Anzeige Aus                 

RET

;________________________________________________________________________________________

;Tabelle für Punktausgabe
tab: DB 00000000b, 10000000b, 11000000b, 11100000b, 11110000b, 11111000b

